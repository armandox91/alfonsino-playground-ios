//
//  UIButtonExtension.swift
//  AlfonsinoPlayground
//
//  Created by Antongiulio Pellegrino on 25/02/2020.
//  Copyright © 2020 Alfonsino Srl. All rights reserved.
//

import UIKit

extension UIButton {
    //Dato che nel progetto ci sono 2 bottoni con gli stessi stili, ho esteso UIButton con un metodo che applica gli stili definiti nell'esercizio, in modo da applicare rapidamente lo stile ai bottoni
    func loadAlfonsinoStyle() {
        //Per il background mi sono servito di un metodo definto nell'extension di UIColor (UIColorExtension)
        backgroundColor = UIColor.colorWith(hex: "#0079FF")
        setTitleColor(UIColor.white, for: .normal)
        titleLabel?.font = UIFont.boldSystemFont(ofSize: 14.0)
        layer.cornerRadius = 8.0
        layer.borderWidth = 2.0
        layer.borderColor = UIColor.black.cgColor
    }
}
