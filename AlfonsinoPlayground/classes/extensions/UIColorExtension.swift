//
//  UIColorExtension.swift
//  AlfonsinoPlayground
//
//  Created by Antongiulio Pellegrino on 04/03/2020.
//  Copyright © 2020 Alfonsino Srl. All rights reserved.
//

import UIKit

extension UIColor {
    
    //Ho esteso UIColor e implementato questo metodo di comodo che converte automaticamente un colore in esadecimale, ritornando direttamente un UIColor. Gestisce sia l'esadecimale con il cancelletto davanti, sia senza. Nel caso in cui l'esadecimale non abbia i 6 caratteri che si aspetta, tornerà un grigio
    static func colorWith(hex: String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        if ((cString.count) != 6) {
            return UIColor.gray
        }
        var rgbValue:UInt64 = 0
        Scanner(string: cString).scanHexInt64(&rgbValue)
        return UIColor(red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0, green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0, blue: CGFloat(rgbValue & 0x0000FF) / 255.0, alpha: CGFloat(1.0)
        )
    }
}
