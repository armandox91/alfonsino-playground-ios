//
//  UIViewControllerExtension.swift
//  AlfonsinoPlayground
//
//  Created by Antongiulio Pellegrino on 04/03/2020.
//  Copyright © 2020 Alfonsino Srl. All rights reserved.
//

import UIKit

extension UIViewController {
    //Dato che nel progetto ci sono diversi loader, ho esteso UIViewController e implementato 2 metodi di comodo per mostrare e nascondere un loader ed una dimmingView semitrasparente che impedisca all'utente l'interazione
    
    func showLoader() {
        //Istanzio una view con la stessa dimensione della view del controller sul quale mostriamo il loader
        let vwDimming = UIView(frame: view.bounds)
        //Setto le autoresizinkmasks per fare in modo che si adatti in caso di rotazione. Le autoresizingMask su view istanziate programmaticamente vengono automaticamente trasformate a runtime in layout constraints
        vwDimming.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        //Background color nero con alpha a 0.3
        vwDimming.backgroundColor = UIColor.black.withAlphaComponent(0.3)
        //Setto un tag alla view in modo da potermela ripescare senza dover definire variabili globali o altre porcate. E' un modo rudimentale, ma efficace e abbastanza performante. Il motivo di questa riga sarà più chiaro nel metodo successivo (hideLoader). Il 1926 è un chiaro riferimento all' unica squadra di calcio che merita di essere tifata.
        vwDimming.tag = 1926
        
        //Istanzio il loader di sistema, faccio partire il loading e lo posiziono al centro della dimming view appena istanziata
        let vwActivityIndicator = UIActivityIndicatorView(style: .large)
        vwActivityIndicator.startAnimating()
        vwActivityIndicator.center = vwDimming.center
        
        DispatchQueue.main.async {
            //Aggiungo il loader alla dimming e la dimming alla view del viewController
            vwDimming.addSubview(vwActivityIndicator)
            self.view.addSubview(vwDimming)
        }
    }
    
    func hideLoader() {
        DispatchQueue.main.async {
            //Grazie al parametro tag settato nella showLoader mi filtro le subviews della view del nostro ViewController, finchè non trovo la dimmingView
            let subview = self.view.subviews.first { (subview) -> Bool in
                return subview.tag == 1926
            }
            
            //Se ho un riferimento alla dimming view, lo rimuovo dalla superview (quindi nasconde il loader e lo dealloca in quanto non più referenziato da nessuno)
            if let vwDimming = subview {
                vwDimming.removeFromSuperview()
            }
        }
    }
}
