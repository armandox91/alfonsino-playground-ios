//
//  UIAlertControllerExtension.swift
//  AlfonsinoPlayground
//
//  Created by Antongiulio Pellegrino on 04/03/2020.
//  Copyright © 2020 Alfonsino Srl. All rights reserved.
//

import UIKit

extension UIAlertController {
    //Dato che nel progetto ci sono diversi alert, ho esteso UIAlertController con un metodo di comodo custom che velocizza l'init di un alert
    static func buildAlert(title: String = "Attenzione", message: String, showCancelButton: Bool = false, mainActionTitle: String = "Ok", mainAction: ((UIAlertController)->Void)? = nil) -> UIAlertController {
        //Istanzio l'alert coi parametri title e message (title è opzionale, se non presente setterà automaticamente "Attenzione"
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        //Se in input abbiamo questo flag a true, aggiunge di base un bottone "Annulla" che dismissa semplicemente l'alert
        if showCancelButton {
            alert.addAction(UIAlertAction(title: "Annulla", style: .cancel, handler: nil))
        }
        
        //Aggiunge il bottone definito coi parametri di input. Il testo sarà il contenuto di "mainActionTitle", mentre al tap sul bottone eseguirà la closure  "mainAction"
        alert.addAction(UIAlertAction(title: mainActionTitle, style: .default, handler: { (act) in
            mainAction?(alert)
        }))
        
        //Ritorno l'alert appena costruito
        return alert
    }
}
