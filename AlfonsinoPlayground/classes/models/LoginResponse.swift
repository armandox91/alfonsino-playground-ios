//
//  LoginResponse.swift
//  AlfonsinoPlayground
//
//  Created by Antongiulio Pellegrino on 05/03/2020.
//  Copyright © 2020 Alfonsino Srl. All rights reserved.
//

import Foundation

struct LoginResponse: Decodable {
    let access_token : String?
}
