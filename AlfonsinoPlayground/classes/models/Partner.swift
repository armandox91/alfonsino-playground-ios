//
//  Partner.swift
//  AlfonsinoPlayground
//
//  Created by Antongiulio Pellegrino on 04/03/2020.
//  Copyright © 2020 Alfonsino Srl. All rights reserved.
//

import Foundation

struct Partner: Decodable {
    var id: Int
    var title: String
    var img: String
    var created_at: String
}
