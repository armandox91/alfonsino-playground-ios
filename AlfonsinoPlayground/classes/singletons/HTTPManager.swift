//
//  HTTPManager.swift
//  AlfonsinoPlayground
//
//  Created by Antongiulio Pellegrino on 25/02/2020.
//  Copyright © 2020 Alfonsino Srl. All rights reserved.
//

import Foundation

class HTTPManager {
    
    //BaseURL per tutte le chiamate http
    static let baseURL = "https://playground.alfonsino.delivery/api/"
    
    //Enum per tutti gli endpoint utilizzati
    enum HTTPEndPoint : String {
        case login = "auth/login" //GET
        case partners = "partners" //GET, POST
    }
    
    //Metodo di accesso al singleton
    static let shared = HTTPManager()

    //Tipi di closures
    typealias HTTPManagerResponse = (Bool,Any?) -> Void //Params: Success,Data/Error
    typealias HTTPLoginResponse = (Bool,LoginResponse?) -> Void //Params: Success,AccessToken
    typealias HTTPGetPartnersResponse = (Bool,[Partner]?) -> Void //Params: Success,Partners

    //Metodo generico per tutte le HTTPRequests (con flag authenticate che aggiunge l'header col jwt se presente e con completion closure generica)
    func executeRequest(request: URLRequest, authenticate: Bool = true, completion: @escaping HTTPManagerResponse) {
        //Istanzio una urlSession con configurazione di default
        let session = URLSession(configuration: URLSessionConfiguration.default)
        //Mi copio la request da eseguire in una variabile in modo da poter aggiungere eventuali header generici
        var req = request
        //Aggiunge l'header Content-Type che è uguale per tutte le chiamate http del progetto
        req.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        //Se il flag authenticate è true aggiunge l'header Authorization con il token jwt se presente
        if authenticate {
            //Prendo il token utilizzando il singleton AuthToken
            if let token = AuthToken.shared.getToken() {
                req.addValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
            }
        }
        
        //Istanzio il data task dando in input la request
        let task = session.dataTask(with: req) { (data, response, error) in
            //Closure del datatask, verifico l'esito
            guard error == nil else {
                //In questo caso ho esito negativo, quindi chiamo direttamente la completion con success false e, se presente, l'errore
                completion(false,error)
                return
            }
            
            //La chiamata non ha errori, quindi mi popolo una variabile con lo statusCode
            let statusCode = (response != nil && response is HTTPURLResponse) ? (response as! HTTPURLResponse).statusCode : 400
            
            //Verifico che lo statusCode sia tra 200 e 299 (success)
            guard statusCode >= 200 && statusCode < 300 else {
                //Status code non incluso nel range di success, quindi chiamo il completion con success false e il body della response
                completion(false,data)
                return
            }
            
            //La chiamata è riuscita, chiamo il completion con success true e il body della response
            completion(true,data)
        }
        task.resume()
    }
    
    //MARK: Metodi delle singole chiamate HTTP
    
    //Chiamata http per la login. Prende in input i parametri email, password e una closure di completion personalizzata per questa chiamata
    func login(email: String, password: String, completion: @escaping HTTPLoginResponse) {
        //Compongo l'url servendomi del baseURL e dell'enum definito all'inizio
        let url = URL(string: HTTPManager.baseURL + HTTPEndPoint.login.rawValue)
        //Istanzio una urlrequest con l'url appena costruito
        var request = URLRequest(url: url!)
        //Setto il metodo della chiamata http
        request.httpMethod = "POST"
        //Istanzio un dictionary con i parametri da passare nel body della request
        let params : [AnyHashable:Any] = ["email":email,"password":password]
        //Provo a trasformare il dictionary dei parametri in un oggetto di tipo Data
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: params)
            //è riuscito a generare l'oggetto Data, quindi lo setta come httpBody della request
            request.httpBody = jsonData
        } catch {
            //non è riuscito a generare l'oggetto Data, quindi chiama la completion con success false e nessun oggetto response
            completion(false,nil)
            return
        }
        
        //Esegue la chiamata http senza autenticarla (ovviamente la login non richiede il token jwt)
        executeRequest(request: request, authenticate: false) { (success, data) in
            guard success else {
                //Chiamata di login fallita, quindi setto a nil il token e chiamo il completion con success false e nessun dato
                AuthToken.shared.setToken(token: nil)
                DispatchQueue.main.async {
                    completion(false,nil)
                }
                return
            }
            
            guard data != nil && data is Data else {
                //La chiamata è in success, ma per qualche motivo non ci è arrivato alcun dato, quindi la login è fallita. Setto a nil il token e chiamo il completion con success false e nessun dato
                AuthToken.shared.setToken(token: nil)
                DispatchQueue.main.async {
                    completion(false,nil)
                }
                return
            }
            
            //Provo a serializzare il body della response per ottenere un oggetto di tipo LoginResponse (che conterrà l'accessToken)
            do {
                let response = try JSONDecoder().decode(LoginResponse.self, from: data as! Data)
                //E' riuscito a serializzare la response, quindi salvo l'access token e chiamo il completion con success e l'oggetto LoginResponse
                AuthToken.shared.setToken(token: response.access_token)
                DispatchQueue.main.async {
                    completion(success,response)
                }
            } catch {
                //Non è riuscito a serializzare la response, quindi setto a nil il token e chiamo il completion con success false e nessun dato
                AuthToken.shared.setToken(token: nil)
                DispatchQueue.main.async {
                    completion(false,nil)
                }
            }
        }
    }
    
    //Chiamata http per ottenere i partners. La chiamata gestisce la paginazione, infatti prende in input un parametro skip (che di default è 0), quindi il server tornerà i successivi X partner (X è definito lato server) partendo dal partner n° "skip". Prende in input anche una closure di completion ad hoc per la getPartners
    func getPartners(skip: Int = 0, completion: @escaping HTTPGetPartnersResponse) {
        //Compongo l'url servendomi del baseURL e dell'enum definito all'inizio. Aggiungo in get il parametro skip
        let url = URL(string: HTTPManager.baseURL + HTTPEndPoint.partners.rawValue + "?skip=\(skip)")
        //Istanzio una urlrequest con l'url appena costruito
        var request = URLRequest(url: url!)
        //Setto il metodo della chiamata http
        request.httpMethod = "GET"
        //Esegue la chiamata http autenticandola
        executeRequest(request: request, authenticate: true) { (success, data) in
            guard success && data != nil && data is Data else {
                //Chiamata di get partners fallita, quindi chiamo il completion con success false e nessun dato
                DispatchQueue.main.async {
                    completion(false,nil)
                }
                return
            }
            
            //Provo a serializzare il body della response per ottenere un array di oggetti di tipo Partner
            do {
                let partners = try JSONDecoder().decode([Partner].self, from: data as! Data)
                //E' riuscito a serializzare la response, quindi chiamo il completion con success e l'array di Partners appena serializzato
                DispatchQueue.main.async {
                    completion(true,partners)
                }
            } catch {
                //Non è riuscito a serializzare la response, quindi chiamo il completion con success false e nessun dato
                DispatchQueue.main.async {
                    completion(false,nil)
                }
            }
        }
    }
}
