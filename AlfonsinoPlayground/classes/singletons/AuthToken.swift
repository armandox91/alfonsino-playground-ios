//
//  AuthToken.swift
//  AlfonsinoPlayground
//
//  Created by Antongiulio Pellegrino on 04/03/2020.
//  Copyright © 2020 Alfonsino Srl. All rights reserved.
//

import Foundation

//Classe di comodo per la gestione dell'access token su disco
class AuthToken {
    //Metodo di accesso al singleton
    static let shared = AuthToken()
    //Lazy instantiation dell'attributo access token. Può essere nil e la prima volta viene popolato da UserDefaults
    private lazy var accessToken : String? = {
        return UserDefaults.standard.value(forKey: "access_token") as? String
    }()
    
    //Setter pubblico del token, che storicizza il token sia sul disco (UserDefaults), sia in memoria (property accessToken)
    func setToken(token: String?) {
        UserDefaults.standard.set(token, forKey: "access_token")
        accessToken = token
    }
    
    //Getter pubblico del token
    func getToken() -> String? {
        return accessToken
    }
}
