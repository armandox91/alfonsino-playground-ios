//
//  ViewController.swift
//  AlfonsinoPlayground
//
//  Created by Antongiulio Pellegrino on 25/02/2020.
//  Copyright © 2020 Alfonsino Srl. All rights reserved.
//

import UIKit
import SDWebImage

class LoginViewController: UIViewController, UITextFieldDelegate {
    @IBOutlet var btnLogin : UIButton!
    @IBOutlet var txtMail : UITextField!
    @IBOutlet var txtPass : UITextField!
    @IBOutlet var ivwLogo : UIImageView!
    
        override func viewDidLoad() {
            super.viewDidLoad()
            navigationController?.setNavigationBarHidden(true, animated: false)
            
            //Applico al bottone di login gli stili definiti nell'extension di UIButton
            btnLogin.loadAlfonsinoStyle()
            
            //Carico il logo di alfonsino da remoto utilizzando la libreria SDWebImage (aggiunta tramite cocoapods)
            ivwLogo.sd_setImage(with: URL(string: "https://playground.alfonsino.delivery/logo.png"), completed: nil)
            
            //Do il focus alla textfield della email, in modo da avviare il controller di login con la tastiera aperta
            txtMail.becomeFirstResponder()
            
            //Istanzio ed aggiungo un tapgesture sulla view, in modo da chiudere la tastiera se questa è aperte e l'utente preme al di fuori della tastiera stessa
            let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard))
            self.view.addGestureRecognizer(tapGesture)
        }
        
        //MARK: UITextFieldDelegate callbacks
        
        func textFieldShouldReturn(_ textField: UITextField) -> Bool {
            //se premo il return button della tastiera controllo quale textfield ha il focus
            if textField == txtMail {
                //se il focus è sul campo Email, passa il focus al campo Password
                txtPass.becomeFirstResponder()
            } else {
                //Se ho il focus sul campo Password, dismissa la tastiera ed esegui l'azione di login
                txtPass.resignFirstResponder()
                btnLogin_action()
            }
            return true
        }
        
        //MARK: Actions
        
        @IBAction func btnLogin_action() {
            let email = txtMail.text
            let password = txtPass.text
            
            //Verifico che il campo email non sia vuoto, altrimenti comunico all'utente il messaggio ed evito la chiamata al server
            guard email != nil && email!.count > 0 else {
                //buildAlert è una funzione definita in un'extension di UIAlertController
                present(UIAlertController.buildAlert(message: "Devi compilare il campo email."), animated: true)
                return
            }
            
            //Verifico che il campo email contenga una mail valida, altrimenti comunico all'utente il messaggio ed evito la chiamata al server
            guard isValidEmail(email: email) else {
                //buildAlert è una funzione definita in un'extension di UIAlertController
                present(UIAlertController.buildAlert(message: "Devi inserire una email valida."), animated: true)
                return
            }
            
            //Verifico che il campo password non sia vuoto, altrimenti comunico all'utente il messaggio ed evito la chiamata al server
            guard password != nil && password!.count > 0 else {
                //buildAlert è una funzione definita in un'extension di UIAlertController
                present(UIAlertController.buildAlert(message: "Devi compilare il campo password."), animated: true)
                return
            }
            
            //Mostro il loader (Vedi extension di UIViewController)
            showLoader()
            HTTPManager.shared.login(email: email!, password: password!) { [weak self] (success, token) in
                //Nascondo il loader (Vedi extension di UIViewController)
                self?.hideLoader()
                guard success else {
                    //In questo caso la chiamata http è fallita, quindi mostriamo un alert di errore
                    //buildAlert è una funzione definita in un'extension di UIAlertController
                    self?.present(UIAlertController.buildAlert(message: "Autenticazione fallita. Controlla i campi e riprova."), animated: true)
                    return
                }
                
                //In questo caso la chiamata http di login è andata a buon fine, quindi istanzio il controller dei partner e lo pusho sul navigation controller
                if let partnersVC = PartnersViewController.instantiateFromStoryboard() {
                    self?.navigationController?.pushViewController(partnersVC, animated: true)
                }
            }
        }
        
        //Metodo che dismissa la tastiera. Viene chiamato quando l'utente fa tap al di fuori della tastiera
        @objc func dismissKeyboard() {
            self.view.endEditing(true)
        }
        
        //MARK: Utils
        
        func isValidEmail(email: String?) -> Bool {
            //Questa funzione verifica che la mail sia valorizzata, che contenga una @ e un .
            guard email != nil else { return false }
            guard email!.contains("@") else { return false }
            guard email!.contains(".") else { return false }
            return true
        }
    }
