//
//  PartnersViewController.swift
//  AlfonsinoPlayground
//
//  Created by Antongiulio Pellegrino on 04/03/2020.
//  Copyright © 2020 Alfonsino Srl. All rights reserved.
//

import UIKit

class PartnersViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet var tableView : UITableView!
    @IBOutlet var btnAddPartner : UIButton!
    
    var dataset = [Partner]()
    var skip : Int = 0
    var skipFinished = false
    
    //Metodo di comodo per istanziare il PartnerViewController programmaticamente, ma da storyboard
    static func instantiateFromStoryboard() -> PartnersViewController? {
        let cnt = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(identifier: String(describing: PartnersViewController.self)) as? PartnersViewController
        return cnt
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //Applico al bottone di aggiunta partner gli stili definiti nell'extension di UIButton
        btnAddPartner.loadAlfonsinoStyle()
        
        //Setto un contentInset bottom alla tableView per far si che il bottone di aggiunta partner non si sovrapponga all'ultima cella della tableView
        tableView.contentInset = UIEdgeInsets(top: 0.0, left: 0.0, bottom: 50.0, right: 0.0)
        
        //Effettuo la prima chiamata http per caricare i primi partner
        loadPartners()
    }
    
    //MARK: TableViewDelegate&TableViewDataSource callbacks
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataset.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //Riutilizzo la cella definita come prototype nello storyboard
        if let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: PartnerTableViewCell.self), for: indexPath) as? PartnerTableViewCell {
            //prendo un riferimento al partner per popolare la cella
            let item = dataset[indexPath.row]
            //chiamo una funzione di comodo della cella per valorizzare immagine e testo prendendo in input un partner
            cell.setup(partner: item)
            return cell
        }
        //Non si verificherà mai, ma l'ho messo per eccesso di zelo
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        //Se l'ultima chiamata http di get partners ha tornato almeno 1 partner e il sistema sta per mostrare a schermo l'ultima cella, faccio partire una loadPartner per caricare i successivi partner. Il controllo serve per evitare che, una volta caricati tutti i partner, continui ad effettuare chiamate http inutili quando si scorre in fondo alla tableview
        if !skipFinished && indexPath.row == dataset.count - 1 {
            loadPartners()
        }
    }
    
    //MARK: Actions
    
    @IBAction func btnAddPartner_action() {

    }
    
    //MARK: Network
    
    //Questo metodo effettua una chiamata http get per ottenere i partner. Il metodo gestisce internamente la paginazione, che viene azzerata passando un booleano opzionale "reset", che si occupa di svuotare i partner caricati fin ora e di azzerare il count "skip" col quale gestiamo la paginazione
    func loadPartners(reset: Bool = false) {
        //Mostro il loader (Vedi extension di UIViewController)
        showLoader()
        
        //Se il parametro reset (che di default è false) è true, azzera il parametro skip
        if reset {
            skip = 0
        }
        
        //Effettuo la chiamata http di get dei partners, dando in input il parametro skip
        HTTPManager.shared.getPartners(skip: skip) { [weak self] (success, partners) in
            //Nascondo il loader (Vedi extension di UIViewController)
            self?.hideLoader()
            
            //In questo caso la chiamata http è fallita, quindi ritorniamo senza far nulla
            guard success && partners != nil else {
                return
            }
            
            //In questo caso la chiamata http è riuscita, quindi ci occupiamo di aggiornare i counters, il dataset e ricarichiamo la tableView
            
            //Se la chiamata è stata effettuata col flag reset, svuoto il dataset dai partners caricati nelle chiamate precedenti. Lo faccio qui e non dove azzero skip per sicurezza (se svuoto il dataset prima della chiamata http ci sono casi in cui potrebbe crashare perchè parte un cellForRowAtIndexPath dopo aver svuotato il dataset, quindi crasherebbe)
            if reset {
                self?.dataset.removeAll()
            }
            
            //Incremento il parametro skip (per la paginazione) del numero di partners appena ottenuti
            self?.skip += partners!.count
            //Se la chiamata mi ha tornato 0 partner vuol dire che la paginazione è terminata, quindi popolo questo flag in modo da non far partire  chiamate http inutili
            self?.skipFinished = partners!.count == 0
            //Aggiungo in append i nuovi dati al dataset
            self?.dataset.append(contentsOf: partners!)
            //Ricarico la tableView
            self?.tableView.reloadData()
        }
    }
}

class PartnerTableViewCell: UITableViewCell {
    @IBOutlet var ivw : UIImageView!
    @IBOutlet var lbl : UILabel!
    
    //Metodo di comodo per fillare label e imageView partendo da un oggetto Partner
    func setup(partner: Partner) {
        lbl.text = partner.title
        //Carico l'immagine del partner da remoto utilizzando la libreria SDWebImage (aggiunta tramite cocoapods)
        ivw.sd_setImage(with: URL(string: partner.img), completed: nil)
    }
}
