//
//  AppDelegate.swift
//  AlfonsinoPlayground
//
//  Created by Antongiulio Pellegrino on 25/02/2020.
//  Copyright © 2020 Alfonsino Srl. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        return true
    }
}

